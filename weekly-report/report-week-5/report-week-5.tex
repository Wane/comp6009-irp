\documentclass[12pt,journal,compsoc]{IEEEtran}

\usepackage{comment}
\usepackage[nocompress]{cite}
\usepackage[pdftex]{graphicx}
\graphicspath{{pic/}}
\usepackage{braket}
\hyphenation{op-tical net-works semi-conduc-tor}

\begin{document}
\title{\bf IRP Weekly Report\\Week 5}
\author{Weike~Liao,~\IEEEmembership{Student,}
        Soon~X.~Ng,~\IEEEmembership{Supervisor}
\IEEEcompsocitemizethanks{\IEEEcompsocthanksitem W. Liao is with the School of Electronics and Computer Science, University of Southampton, United Kindom.\protect\\
% note need leading \protect in front of \\ to get a newline within \thanks as
% \\ is fragile and will error, could use \hfil\break instead.
E-mail: wl15g09@ecs.soton.ac.uk
\IEEEcompsocthanksitem S.X. Ng is with the group of Communications, Signal Processing and Control, School of Electronics and Computer Science, University of Southampton, United Kindom.\protect\\
Email: sxn@ecs.soton.ac.uk}}% <-this % stops a space

\IEEEcompsoctitleabstractindextext{%
\begin{abstract}
%\boldmath
The abstract goes here.
\end{abstract}
% IEEEtran.cls defaults to using nonbold math in the Abstract.
% This preserves the distinction between vectors and scalars. However,
% if the journal you are submitting to favors bold math in the abstract,
% then you can use LaTeX's standard command \boldmath at the very start
% of the abstract to achieve this. Many IEEE journals frown on math
% in the abstract anyway. In particular, the Computer Society does
% not want either math or citations to appear in the abstract.

% Note that keywords are not normally used for peerreview papers.
\begin{IEEEkeywords}
Quantum teleportation, entanglement, simulation, mechanics, evaluation
\end{IEEEkeywords}}

\maketitle
\section{Introduction}
\paragraph*{}
\IEEEPARstart{T}{he} subject of quantum information theory (QIT) nowadays is a fast developing area with increasing investigations and researches around the globe. In this report we will focus on a subarea in QIT named quantum teleportation (QT)\cite{quantum-network,quantum-teleportation-100,quantum-teleportation-143,quantum-computing,qt-epr}. The objective of QT is to transmit an unknown quantum state over a distance, and it is one of the few quantum communication protocols invented so far (another one is superdense coding)\cite{quantum-network}. 
\paragraph*{}
The theoretical report was first published in 1993 by six scientists\cite{qt-epr}. Say we have a pair of entangled quanta\cite{wiki-entangle}, and each is given to Alice and Bob who are at different locations. Alice has another quantum $\ket{\phi}$ with unknown state which she will send to Bob. Alice perform a joint Bell-state measurement on the two quanta she has, and send the measurement result to Bob via some classical communication method. Bob then can perform unitary transformation on his quantum according to the data received, and finally produce a copy of the initial quantum system $\ket{\phi}$. After the measurement process the states of Alice's two quanta are completely disrupted, thus the term ``teleportation". The process is also illustrated in Figure~\ref{fig:qt}. 
\begin{figure}[here]
\centering
\includegraphics[keepaspectratio=true,width=13cm]{qt.png}
\caption{Quantum teleportation}
\label{fig:qt}
\end{figure}
\paragraph*{}
Following that discovery other scientists realised the idea and demonstrated QT experiments in various systems such as photons, nuclear spins and trapped ions\cite{qt-ibm,exp-qt}. Photons are found to be easy to manipulate and transmit, and the early methods of distributing photons were mainly through optical fibres. Recent experimental achievements left behind fibres due to their high photon loss as well as decoherence, while realised free-space quantum channels in a maximum distance of 144 kilometers so far\cite{quantum-teleportation-100,quantum-teleportation-143}. 


\section{Motivation and possible applications}
\paragraph*{}
Quantum computing is the foreseeable future of computer science, it utilises quantum mechanical phenomena and relies on quantum information theory. The direct communication in qubits between different individuals is an essential part in distributed quantum computing, which also introduces the need for a global quantum network. However, the sending of quantum states in classical means is impossible, which can be explained in the following arguments\cite{wiki-qt}:
\begin{itemize}
\item Physical transport of qubits. \\
Fragile quantum states would be easily corrupted by very little interruptions during the way, which is not recommended. 
\item Broadcast of quantum information. \\
No-broadcast theorem, a corollary of the no-cloning theorem, states that it is impossible to create a state that both of its parts are the same as the original state. 
\item Measure the quantum going to be sent, then send its information in classical manner and creates a new quantum on the receiver side according to the information. \\
Since we can only perform a single measurement of any quantum before disturbing it, and due to Heisenberg's uncertainty principle, we cannot measure the complete states of a quantum. This is also explained in the no-teleportation theorem\cite{wiki-no-tele}. 
\end{itemize}
Quantum teleportation is the first and only solution so far, it gives the feasibility to transfer unknown quantum states over long distances. The first realistic application of this technology is in quantum cryptography.
\paragraph*{}
Nowadays the most popular and reliable method for data encryption is to separate a private encryption key depending on the data, and transfer the key individually. Quantum cryptography\cite{quantum-crypt} is a rather old field compared to QT, its main idea is to transmit the private key via quantum states, where majority experiments uses polarisation of photons. The most famous protocol, BB84\cite{quantum-crypt}, is a method that calculates private key on both sender and receiver sides, depending on the photon polarisations measured.  It is said to be immune to eavesdropping because interception and retransmission by the eavesdropper can not extract useful data. However, in that case, the transmission will be disturpted and the noise ratio greatly increases. Quantum teleportation solves this problem by completely forbidding eavesdropping and is called ``the ultimate solution to quantum cryptography"\cite{quantum-crypt}. 
\paragraph*{}
Another discovery in quantum teleportation is entanglement swapping. This technique achieves the entanglement of quanta that never interacted. Consider the following case as shown in Figure~\ref{fig:entang-swap} , we have four quanta, $Alice$, $Bob1$, $Bob2$ and $Carol$. $Alice$ and $Bob1$ are entangled and so are $Bob2$ and $Carol$, where $Bob1$ and $Bob2$ are in the same place.  
The idea is, in short, to teleport the state of $Bob1$ to $Carol$ by performing projective measurement on $Bob1$ and $Bob2$, and as a result, $Alice$ and $Carol$ are now entangled\cite{entangle-swap}. 
\begin{figure}[here]
\centering
\includegraphics[keepaspectratio=true,width=9cm]{entang-swap.png}
\caption{Entanglement swapping}
\label{fig:entang-swap}
\end{figure}
\paragraph*{}
In the research of quantum network, QT can be used as a communication protocol at the cost of an EPR pair and two classical bits per qubit\cite{quantum-network}. This requires entanglement distribution by routers between Alice and Bob who do not share direct connections, which means it is possible to establish virtual connections between nodes without a direct quantum channel between them\cite{quantum-network}. 

\section{Challenges}
\paragraph*{}
A major challenge in quantum teleportation is the significant short coherence time between entangled pairs. When a qubit is in superposition, that is, it simutaneously holds values 1 and 0, then it is said to be in coherece. Any perturbation from the environment will result the qubit collapse into a pure state and terminate the entanglement\cite{entang-decoh}. At the current level of research, we have to use entanglement distribution via optical channels, which is a delicate and challenging task in QT, and it is one of the main reasons that make QT not a feasible solution to current quantum communication\cite{quantum-teleportation-143}. This problem will hopefully be solved with the forthcoming technology of quantum memory. In this case, entangled quanta can be stored in separate quantum memories and be kept at different locations for significantly longer time, which brings future QT further advantages: 
\begin{itemize}
\item QT is available even when quantum channel has low quality, or
\item When the location of Bob is unknown, means a broadcast of classical channel from Alice. 
\end{itemize}
\paragraph*{}
Experimental free-space long-distance QT face many difficulties, especially in the manipulation of quantum channel. For instance, one is the extremely low signal-to-noise ratio. To cope with this, a successful experiment in 2012 utilised: frequency-uncorrelated polarization-entangled photon pair source, ultra-low-noise large-active-area single-photon detectors and entanglement-assisted clock synchronisation, while the experiments could only be carried during night\cite{quantum-teleportation-143}. 


\section{Current development}
\paragraph*{}



\begin{comment}




\end{comment}
\appendices
\section{First Appendix}
\section*{Acknowledgments}
The authors would like to thank...
\bibliographystyle{ieeetr}
\bibliography{ref}



\end{document}