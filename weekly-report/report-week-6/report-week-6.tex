\documentclass[11pt,journal,compsoc]{IEEEtran}

\usepackage{comment}
\usepackage{cite}
\usepackage[pdftex]{graphicx}
\graphicspath{{pic/}}
\usepackage{braket}
\hyphenation{real-ised}
\usepackage{hyperref}
\usepackage{breakurl}
\usepackage[outerbars]{changebar}

\begin{document}
\title{\bf IRP Weekly Report\\Week 6}
\author{Weike~Liao,~\IEEEmembership{Student,}
        Soon~X.~Ng,~\IEEEmembership{Supervisor}
\IEEEcompsocitemizethanks{\IEEEcompsocthanksitem W. Liao is with the School of Electronics and Computer Science, University of Southampton, United Kindom.\protect\\
% note need leading \protect in front of \\ to get a newline within \thanks as
% \\ is fragile and will error, could use \hfil\break instead.
E-mail: wl15g09@ecs.soton.ac.uk
\IEEEcompsocthanksitem S.X. Ng is with the group of Communications, Signal Processing and Control, School of Electronics and Computer Science, University of Southampton, United Kindom.\protect\\
Email: sxn@ecs.soton.ac.uk}}% <-this % stops a space

\IEEEcompsoctitleabstractindextext{%
\begin{abstract}
%\boldmath
The abstract goes here.
\end{abstract}
% IEEEtran.cls defaults to using nonbold math in the Abstract.
% This preserves the distinction between vectors and scalars. However,
% if the journal you are submitting to favors bold math in the abstract,
% then you can use LaTeX's standard command \boldmath at the very start
% of the abstract to achieve this. Many IEEE journals frown on math
% in the abstract anyway. In particular, the Computer Society does
% not want either math or citations to appear in the abstract.

% Note that keywords are not normally used for peerreview papers.
\begin{IEEEkeywords}
quantum, teleportation, entanglement, simulation, mechanics, evaluation
\end{IEEEkeywords}}

\maketitle
\section{Introduction}
\IEEEPARstart{T}{he} subject of quantum information theory (QIT) nowadays is a fast developing area with increasing investigations and researches around the globe. In this report we will focus on a subarea in QIT named quantum teleportation (QT)\cite{quantum-network,quantum-teleportation-100,quantum-teleportation-143,quantum-computing,qt-epr}. The objective of QT is to transmit an unknown quantum state over a distance, and it is one of the few quantum communication protocols invented so far (another one is superdense coding)\cite{quantum-network}. 

The theoretical report was first published in 1993 by six scientists\cite{qt-epr}. Say we have a pair of entangled qubits\cite{wiki-entangle}, and each is given to Alice and Bob who are at different locations. Alice has another qubit $\ket{\phi}$ with unknown state which she will send to Bob. Alice perform a joint Bell-state measurement on the two qubits she has, and send the measurement result to Bob via some classical communication method. Bob then can perform unitary transformation on his qubit according to the data received, and finally produce a copy of the initial quantum system $\ket{\phi}$. After the measurement process the states of Alice's two qubits are completely disrupted, thus the term ``teleportation". The process is also illustrated in Figure~\ref{fig:qt}. 
 
\begin{figure}[here]
\centering
\includegraphics[keepaspectratio=true,height=7cm]{qt.png}
\caption{Quantum teleportation}
\label{fig:qt}
\end{figure}

Following that discovery other scientists realised the idea and demonstrated QT experiments in various systems such as photons, nuclear spins and trapped ions\cite{qt-ibm,exp-qt}. Photons are found to be easy to manipulate and transmit, and the early methods of distributing photons were mainly through optical fibres. Recent experimental achievements left behind fibres due to their high photon loss as well as decoherence, with the successor of free-space quantum channels in a maximum distance of 144 kilometers so far\cite{quantum-teleportation-100,quantum-teleportation-143}. 


\section{Motivation and possible applications}

Quantum computing is the foreseeable future of computer science, it utilises quantum mechanical phenomena and relies on quantum information theory. The direct communication in qubits between different individuals is an essential part in distributed quantum computing, which also introduces the need for a global quantum network. However, the sending of quantum states in classical means is impossible, which can be explained in the following arguments\cite{wiki-qt}:
\begin{itemize}
\item Physical transport of qubits. \\
Fragile quantum states would be easily corrupted by very little interruptions during the way, which is not recommended. 
\item Broadcast of quantum information. \\
No-broadcast theorem, a corollary of the no-cloning theorem, states that it is impossible to create a state that both of its parts are the same as the original state. 
\item Measure the quantum going to be sent, then send its information in classical manner and creates a new quantum on the receiver side according to the information. \\
Since we can only perform a single measurement of any quantum before disturbing it, and due to Heisenberg's uncertainty principle, we cannot measure the complete states of a quantum. This is also explained in the no-teleportation theorem\cite{wiki-no-tele}. 
\end{itemize}
Quantum teleportation is the first and only solution so far, it gives the feasibility to transfer unknown quantum states over long distances. The first realistic application of this technology is in quantum cryptography.

Nowadays the most popular and reliable method for data encryption is to separate a private encryption key depending on the data, and transfer the key individually. Quantum cryptography\cite{quantum-crypt} is a rather old field compared to QT, its main idea is to transmit the private key via quantum states, where majority experiments uses polarisation of photons. The most famous protocol, BB84\cite{quantum-crypt}, is a method that calculates private key on both sender and receiver sides, depending on the photon polarisations measured.  It is said to be immune to eavesdropping because interception and retransmission by the eavesdropper can not extract useful data. However, in that case, the transmission will be disturpted and the noise ratio greatly increases. Quantum teleportation solves this problem by completely forbidding eavesdropping and is called ``the ultimate solution to quantum cryptography"\cite{quantum-crypt}. 

Another discovery in quantum teleportation is entanglement swapping. This technique achieves the entanglement of quanta that never interacted. \cbstart Consider the following case as shown in Figure~\ref{fig:entang-swap}, we have four qubits, $\ket{\phi}_1$, $\ket{\phi}_2$, $\ket{\psi}_1$ and $\ket{\psi}_2$ distributed as the graph illustrates. $\ket{\phi}_1$ and $\ket{\phi}_2$ are entangled and so are $\ket{\psi}_1$ and $\ket{\psi}_2$, where $\ket{\phi}_2$ and $\ket{\psi}_1$ are in the same place.  
The idea is, in short, to teleport the state of $\ket{\phi}_2$ to $\ket{\psi}_2$ by performing projective measurement (Bell-state measurement\cite{bell-measure}) on $\ket{\phi}_2$ and $\ket{\psi}_1$. As a result, $\ket{\phi}_1$ and $\ket{\psi}_2$ are now entangled\cite{entangle-swap}. \cbend \\
\begin{figure}[here]
\centering
\includegraphics[keepaspectratio=true,width=9cm]{entang-swap.png}
\caption{Entanglement swapping}
\label{fig:entang-swap}
\end{figure}

In the research of quantum network, QT can be used as a communication protocol at the cost of an EPR pair and two classical bits per qubit\cite{quantum-network}. This requires entanglement distribution by routers between Alice and Bob who do not share direct connections, which means it is possible to establish virtual connections between nodes without a direct quantum channel between them\cite{quantum-network}. 

\section{Challenges}

A major challenge in quantum teleportation is the significant short coherence time between entangled pairs. When a qubit is in superposition, that is, it simutaneously holds values 1 and 0, then it is said to be in coherece. Any perturbation from the environment will result the qubit collapse into a pure state and terminate the entanglement\cite{entang-decoh}. At the current level of research, we have to use entanglement distribution via optical channels, which is a delicate and challenging task in QT, and it is one of the main reasons that make QT not a feasible solution to current quantum communication\cite{quantum-teleportation-143}. This problem will hopefully be solved with the forthcoming technology of quantum memory. In this case, entangled quanta can be stored in separate quantum memories and be kept at different locations for significantly longer time, which brings future QT further advantages: 
\begin{itemize}
\item QT is available even when quantum channel has low quality, or
\item When the location of Bob is unknown, means a broadcast of classical channel from Alice. 
\end{itemize}

Experimental free-space long-distance QT face many difficulties, especially in the manipulation of quantum channel. For instance, one is the extremely low signal-to-noise ratio. To cope with this, a successful experiment in 2012 utilised: frequency-uncorrelated polarization-entangled photon pair source, ultra-low-noise large-active-area single-photon detectors and entanglement-assisted clock synchronisation, while the experiments could only be carried during night\cite{quantum-teleportation-143}. 

\cbstart
\section{History and current development}
Starting with the fisrt discovery of quantum teleportation by Bennett \emph{et al}\cite{qt-epr}, significant amount of research has been done in the relevant areas in all over the world. It is worth mention that theoretical discoveries are far beyond what have been achieved experimentally. The first experimental QT\cite{exp-qt} happened in 1997, they used photon pairs entangled in polarisation. At that time the techiques for any kind of entanglement were unmature, for which they used a method called type-II parametric down-conversion that produces entangled photons in orthogonal polarisations\cite{para-down-conv}. However in that experiment they did not use classical information at the Bob's side, that means only one case in the four possible Bell-state measurement outcomes is considered (the case where Bob need to apply an {\bf I} gate). In the same year a theoretical report\cite{qt-conti-var} explained the process to teleport continuous spectra (``manifestly quantum sates of electromagnetic field"), with two ancillary highly squeezed two-mode eletromagnetic fields as the entangled states, which was a valuable step in QT from discrete-variable systems to continuous-variable systems. They also managed to obtain accurate fidelity of entanglement in an infinite dimentional Hilbert space with absolute efficiency, compared to parametric down-conversion which only has rare success rate. In the following year a method of QT with a Greenberger-Horne-Zeilinger state triplet\cite{ghz} was published\cite{qt-3-entang}. In that experiment three distant entities were involved, and one can teleport an unknown state to either of other two places. That technique was further expanded in 2004\cite{multi-qt}. The authors formulated a technique to teleport strings of qubits from one location to another via a many-agent network. A notable improvement is that, regardless of the amount of qubits to be teleported, every agent in the network only need one GHZ state qubit, perform one Hadamard gate and send one classical bit to the receiver. \\

The multiqubit teleportation scheme was also explored in the context of secure transmission in that year. A secure direct communication protocol using controlled QT was discovered\cite{qsdc}. The GHZ state triplet was again used, with the third entity \emph{Charlie} as a supervisor of the transmission between \emph{Alice} and \emph{Bob}. As the report's title suggests, this kind of communication does not require secret key distribution, but \emph{Alice} encode her message directly into quantum states $\ket{+}$ and $\ket{-}$. The method is said to be completely secure as long as a perfect quantum channel is provided. A silmilar method uses W state was published in 2006 as well\cite{qsdc-w}. 

\cbend

\begin{comment}
skip quantum channel with quantum memory?

"from discrete-variable systems to continuous-variable
systems and also from a single qubit to multiqubits. On the other hand, recent experiments have demonstrated quantum teleportation with photon-polarized state
, optical coherent states, and nuclear magnetic resonance."\cite{c-tele-mult-qubit}


\end{comment}
\appendices
\section{First Appendix}
\section*{Acknowledgments}
The authors would like to thank...
\bibliographystyle{ieeetr}
\bibliography{ref}



\end{document}