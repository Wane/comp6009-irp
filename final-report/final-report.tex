\documentclass[11pt,journal,compsoc]{IEEEtran}

\usepackage{comment}
\usepackage{cite}
\usepackage[pdftex]{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\graphicspath{{pic/}}
\usepackage{braket}
\usepackage{fancyref}
\usepackage{hyperref}
\usepackage{breakurl}
\usepackage[outerbars]{changebar}
\usepackage{amsmath}
%\usepackage[section]{placeins}

\begin{document}
\title{Investigation and Simulation of \\Quantum Teleportation}
\author{Weike~Liao,~\IEEEmembership{Student,}
        Soon~X.~Ng,~\IEEEmembership{Supervisor}
\IEEEcompsocitemizethanks{\IEEEcompsocthanksitem W. Liao is with the School of
Electronics and Computer Science, University of Southampton, United
Kingdom.\protect\\
E-mail: wl15g09@ecs.soton.ac.uk
\IEEEcompsocthanksitem S.X. Ng is with the group of Communications, Signal
Processing and Control, School of Electronics and Computer Science, University
of Southampton, United Kingdom.\protect\\
Email: sxn@ecs.soton.ac.uk}}

\IEEEcompsoctitleabstractindextext{
\begin{abstract}

In this report the concept of quantum teleportation is introduced, with
explanations about its motivation and possible applications. Previous and
current research achievements are summarised and evaluated in the author's
perspective. A simulation program of quantum teleportation was implemented,
tested and evaluated, giving constructive conclusions in the end.
\end{abstract}

\begin{IEEEkeywords}
quantum, teleportation, entanglement, simulation, mechanics, evaluation
\end{IEEEkeywords}}

\maketitle
\section{Introduction}
\IEEEPARstart{T}{he} subject of quantum information theory (QIT) nowadays is a
fast developing area with increasing investigations and researches around the
globe. In this report we will focus on a subarea in QIT named quantum
teleportation
(QT)\cite{quantum-network,quantum-teleportation-100,quantum-teleportation-143,
quantum-computing,qt-epr}.
The objective of QT is to transmit an unknown quantum state over a distance,
and it is one of the few quantum communication protocols invented so far
(another one is superdense coding)\cite{quantum-network}.

One of the most important elements in transition from classical information
theory to QIT is to understand the concept of quantum bit, or
qubit\cite{qit-book}. A qubit represents the state of a quantum system with
two orthogonal basis: $\ket{0}$ and $\ket{1}$, where the special symbols are
called bra-ket notation\cite{wiki-braket}. An interesting property of qubit is
its
probabilistic superposition of two states, which says it can hold $\ket{0}$
and $\ket{1}$ at the same time with certain probabilities to fall into one of
the
state when is measured. This means, we never know a qubit's true state before
measuring it, while any measurement behaviours will break the superposition
state into a pure state (distinctive $\ket{0}$ or $\ket{1}$). The notation of
a qubit is written as:
\begin{equation} \label{qubit}
\ket{\psi} = \alpha \ket{0} + \beta \ket{1}, 
\end{equation} 
where $\alpha$ and $\beta$ are called qubit amplitudes and $|\alpha|^2+|\beta|^2=1$. 

\begin{figure}[here]
\centering
\includegraphics[keepaspectratio=true,height=7cm]{qt.png}
\caption{Quantum teleportation}
\label{fig:qt}
\end{figure}
The theoretical report of quantum teleportation was first published in 1993 by
six scientists\cite{qt-epr}. They proved the teleportation of a qubit
$\ket{\phi}$ from ``Alice" to ``Bob" is possible by destroying the qubit which
Alice at hand then restore it at Bob's side, without obtaining any information
about the quantum state $\ket{\phi}$. The process can be explained as follows.
Say Alice has three qubits in photon system, in which two of them
$\ket{\psi}_1$ and $\ket{\psi}_2$ are entangled in the Bell
state\cite{wiki-entangle} in polarisation:
\begin{equation} \label{eq:epr-pair}
\ket{\Psi^-}_{12} = \frac{1}{\sqrt{2}}(\ket{\uparrow}_1\ket{\downarrow}_2 -
\ket{\downarrow}_1\ket{\uparrow}_2)
\end{equation}
and another one $\ket{\phi}$ is in an unknown state which she will send its
information to Bob:
\begin{equation} \label{eq:unknown-state}
\ket{\phi} = a\ket{\uparrow} + b\ket{\downarrow}.
\end{equation}
The arrows stands for the polarisation direction of the quantum state, where
an upward arrow means 90$^\circ$ and a downward one means -90$^\circ$. Firstly
Alice sends one of the entangled qubit to Bob via some quantum channel, then
entangle the remaining two qubits by performing a joint Bell-state
measurement\cite{bell-measure}. The complete state of the three qubits is now:
\begin{equation} \label{eq:3-qubits1}
\begin{split} 
\ket{\Psi}_{123} = &
\frac{a}{\sqrt{2}}(\ket{\uparrow}_1\ket{\downarrow}_2\ket{\uparrow}_3 -
\ket{\downarrow}_1\ket{\uparrow}_2\ket{\uparrow}_3) \\ & +
\frac{b}{\sqrt{2}}(\ket{\uparrow}_1\ket{\downarrow}_2\ket{\downarrow}_3 -
\ket{\downarrow}_1\ket{\uparrow}_2\ket{\downarrow}_3).
\end{split}
\end{equation}
Subscript 3 stands for the qubit of the unknown state. According to the Bell
state basis we can then rewrite the equation into:
\begin{equation} \label{eq:3-qubits2}
\begin{split} 
\ket{\Psi}_{123} = \frac{1}{2}[& \ket{\Psi^-}_{13}(-a\ket{\uparrow}_2 -
b\ket{\downarrow}_2) \\ & + \ket{\Psi^+}_{13}(-a\ket{\uparrow}_2 +
b\ket{\downarrow}_2) \\ & + \ket{\Phi^-}_{13} (a\ket{\downarrow}_2 +
b\ket{\uparrow}_2) \\ & + \ket{\Phi^+}_{13}(a\ket{\downarrow}_2 -
b\ket{\uparrow}_2)].
\end{split}
\end{equation}
This explains that, there should be four possible measurement results with
equal probability of 1/4. At this step the qubit $\ket{\psi}_2$, which Bob
has, is supposed to be projected into one of the four pure states shown in
equation~\ref{eq:3-qubits2}. It is not hard to see that $\ket{\psi}_2$ now has
very similar state to the original $\ket{\phi}$, where one of the cases is
exactly the same. In other words, the teleportation is already successful with
25\% chance. With the other three cases we can perform unitary transformations
to $\ket{\psi}_2$ as long as the measurement outcome at Alice's side is
provided, which finally produces an exact replica of the destroyed
$\ket{\phi}$. The process of sending Alice's measurement information is called
active feed-forward, and is usually achieved with a classical data channel.
The whole process of quantum teleportation is also illustrated in
Figure~\ref{fig:qt}.

Following that discovery other scientists realised the idea and demonstrated
QT experiments in various quantum systems such as photons, nuclear spins and trapped
ions\cite{qt-ibm,exp-qt}. Photons are found to have little decoherence from
noisy environment and are easy to manipulate as well as transmit, thus the
early methods of distributing photons were mainly through optical fibres.
Although experiments with fibres were successful, even with cutting-edge
techniques\cite{qt-danube}, optical fibres were still left behind due to their
high photon loss and decoherence factor. Instead free-space optical link was
proved to be the successor, mainly because of its low negative effects from
atmosphere, which brings expressively longer transmission distance (144
kilometres maximum so
far)\cite{exp-fs-qt,quantum-teleportation-100,quantum-teleportation-143}.


\section{Motivation and possible applications}

Quantum computing is the foreseeable future of computer science, it utilises
quantum mechanical phenomena and relies on quantum information theory. The
direct communication in qubits between different individuals is an essential
part in distributed quantum computing, which also introduces the need for a
global quantum network. However, the sending of quantum states in classical
means is impossible, which can be explained in the following
arguments\cite{wiki-qt}:
\begin{itemize}
\item Physical transport of qubits. \\
Fragile quantum states would be easily corrupted by very little interruptions
during the way, which is not recommended.
\item Broadcast of quantum information. \\
No-broadcast theorem, a corollary of the no-cloning theorem, states that it is
impossible to create a state that both of its parts are the same as the
original state.
\item Measure the quantum going to be sent, then send its information in
classical manner and creates a new quantum on the receiver side according to
the information. \\
Since we can only perform a single measurement of any quantum before
disturbing it, and due to Heisenberg's uncertainty principle, we cannot measure
the
complete states of a quantum. This is also explained in the no-teleportation
theorem\cite{wiki-no-tele}.
\end{itemize}
Quantum teleportation is the first and only solution so far, it gives the
feasibility to transfer unknown quantum states over long distances. The first
realistic application of this technology is in quantum cryptography.

Nowadays the most popular and reliable method for data encryption is to
separate a private encryption key depending on the data, and transfer the key
individually. Quantum cryptography\cite{quantum-crypt} is a rather old field
compared to QT, its main idea is to transmit the private key via quantum
states, where majority experiments uses polarisation of photons. The most
famous protocol, BB84\cite{quantum-crypt}, is a method that calculates private
key on both sender and receiver sides, depending on the photon polarisations
measured. It is said to be immune to eavesdropping because interception and
retransmission by the eavesdropper cannot extract useful data. However, in
that case, the transmission will be disturbed and the noise ratio greatly
increases. Quantum teleportation solves this problem by completely forbidding
eavesdropping and is called ``the ultimate solution to quantum
cryptography"\cite{quantum-crypt}.

Apart from quantum key distribution (QKD), the field of quantum secure direct
communication (QSDC) is being actively researched during recent
years\cite{qsdc-superdense,dsdc-entang-swap,qsdc,qsdc-w,qsdc-epr-qt}, where
many of them use teleportation. The QSDC protocol invented by F.G. Deng
\emph{et al.} in 2008\cite{qsdc-superdense} summarised previous techniques and
produced a novel method that uses superdense coding. The protocol inherited
some merits from that in T. Gao \emph{et al.}'s teleportation
protocol\cite{qsdc}, and could be adopted with QT as well. They claimed in
their method that any eavesdropping behaviours will be twice easier to detect
compared to previous BB84 QKD protocol, for the reason that those behaviours
will double the error rate. Details of this protocol will be introduced in
section~\ref{sec:history}.

Another discovery in quantum teleportation is entanglement swapping. This
technique achieves the entanglement of qubits that never interacted. Consider
the following case as shown in Figure~\ref{fig:entang-swap}, we have four
qubits, $\ket{\phi}_1$, $\ket{\phi}_2$, $\ket{\psi}_1$ and $\ket{\psi}_2$
distributed as the graph illustrates. $\ket{\phi}_1$ and $\ket{\phi}_2$ are
entangled and so are $\ket{\psi}_1$ and $\ket{\psi}_2$, where $\ket{\phi}_2$
and $\ket{\psi}_1$ are in the same place.
The idea is, in short, to teleport the state of $\ket{\phi}_2$ to
$\ket{\psi}_2$ by performing projective measurement (Bell-state
measurement\cite{bell-measure}) on $\ket{\phi}_2$ and $\ket{\psi}_1$. As a
result, $\ket{\phi}_1$ and $\ket{\psi}_2$ are now
entangled\cite{entangle-swap}. This technique was applied in quantum
repeater\cite{qurep} and explored to fit with a secure direct communication
protocol\cite{dsdc-entang-swap}.
\begin{figure}[here]
\centering
\includegraphics[keepaspectratio=true,width=9cm]{entang-swap.png}
\caption{Entanglement swapping}
\label{fig:entang-swap}
\end{figure}

In the research of quantum network, QT can be used as a communication protocol
at the cost of an EPR pair and two classical bits per
qubit\cite{quantum-network}. This requires entanglement distribution by
routers between Alice and Bob who do not share direct connections, which means
it is
possible to establish virtual connections between nodes without a direct
quantum channel between them\cite{quantum-network}.

\section{Challenges}

A major challenge in quantum teleportation is the significant short coherence
time between entangled pairs. A qubit is said to be in coherence when
it is in superposition, that is, simultaneously holds values 1 and 0. 
Any
perturbation from the environment will result the qubit collapse into a pure
state and terminate the entanglement\cite{entang-decoh}. At the current level
of research, we have to use entanglement distribution via optical channels,
which is a delicate and challenging task in QT, it is also one of the main
reasons that make QT not a feasible solution to the current field of quantum
communication\cite{quantum-teleportation-143}. This problem will hopefully be
solved with the forthcoming technology of quantum memory. In this case,
entangled quanta can be stored in separate quantum memories and be kept at
different locations for significantly longer time, which brings future QT
further advantages:
\begin{itemize}
\item QT is available even when quantum channel has low quality, or
\item When the location of Bob is unknown, means a broadcast of classical
information from Alice.
\end{itemize}
The current best achievement in practical quantum memory is of 1.75s
decoherence time with 90\% fidelity, which is still far from what QT
requires\cite{quantum-memo-nuclear}. The authors developed a coherence
transfer scheme that involves a processing qubit (electron spin) and a memory
qubit
(nuclear spin). In the writing process, a coherent electron spin state is
generated then stored into a nuclear spin, and vice versa for the reading
process.

Experimental free-space long-distance QT face many difficulties, especially in
the manipulation of quantum channel. For instance, one is the extremely low
signal-to-noise ratio. To cope with this, a successful experiment in 2012
utilised: frequency-uncorrelated polarization-entangled photon pair source,
ultra-low-noise large-active-area single-photon detectors and
entanglement-assisted clock synchronisation, while the experiments could only
be carried out during night\cite{quantum-teleportation-143}.

\section{History and current development} \label{sec:history}
Starting with the first discovery of quantum teleportation by Bennett \emph{et
al}\cite{qt-epr}, significant amount of research has been done in the relevant
areas in all over the world. It is worth mention that theoretical discoveries
are far beyond what have been achieved experimentally.

The first experimental QT\cite{exp-qt} happened in 1997, they used photon
pairs entangled in polarisation. At that time the techniques for any kind of
entanglement were immature, for which they used a method called type-II
parametric down-conversion that produces entangled photons in orthogonal
polarisations\cite{para-down-conv}. However in that experiment they did not
use classical information at the Bob's side, that means only one case in the
four
possible Bell-state measurement outcomes is considered (the case where Bob
need to apply an {\bf I} gate).

In the same year a theoretical report\cite{qt-conti-var} explained the process
to teleport continuous spectra (``manifestly quantum states of electromagnetic
field"), with two ancillary highly squeezed two-mode electromagnetic fields as
the entangled states, which was a valuable step in quantum teleportation from
discrete-variable systems to continuous-variable systems. They also managed to
obtain accurate fidelity of entanglement in an infinite dimensional Hilbert
space with absolute efficiency, compared to parametric down-conversion which
only has rare success rate.

In the following year a method of QT with a Greenberger-Horne-Zeilinger state
triplet\cite{ghz} was published\cite{qt-3-entang}. In that experiment three
distant entities were involved, and one can teleport an unknown state to
either of other two places. That technique was further expanded in
2004\cite{multi-qt}. The authors formulated a technique to teleport strings of
qubits from one location to another via a many-agent network. A notable
improvement was that, regardless of the amount of qubits to be teleported,
every agent in the network only need one GHZ state qubit, perform one Hadamard
gate transform and send one classical bit to the receiver.

The multi-qubit teleportation scheme was also explored in the context of
secure transmission in that year. A quantum secure direct communication (QSDC)
protocol using controlled quantum teleportation was discovered\cite{qsdc}. They
used a triplet of entangled photons which was different from GHZ, and invented
a new triplet-based controlled quantum teleportation protocol, with the third
entity Charlie as a supervisor of the transmission between Alice and Bob. The
new protocol can be explained as following.

Again we have the unknown state to teleport: 
\begin{equation} \label{cqt-u}
\ket{\phi}_U = \alpha \ket{0} + \beta \ket{1}, 
\end{equation} 
and three entangled photons handed to Alice, Bob and Charlie respectively:
\begin{equation} \label {cqt-tri}
\begin{split}
\ket{\xi}_{ABC} = \frac{1}{2}(&\ket{000}+\ket{110} 
+  \ket{011}+\ket{101}).
\end{split}
\end{equation}
If the supervisor Charlie allows the transmission between Alice and Bob, he
would measure his qubit and send the classical one-bit information to the
other two persons, thus giving two possible overall joint states for the four
qubits
involved:
\begin{equation} \label{cqt-joint1}
\begin{split}
\ket{\psi}_{UAB1} = \frac{1}{2}[&\ket{\Phi ^+}_{UA}(\alpha \ket{0} + \beta
\ket{1})_B \\
+ & \ket{\Psi ^+}_{UA}(\alpha \ket{1} + \beta \ket{0})_B \\
+ & \ket{\Phi ^-}_{UA}(\alpha \ket{0} - \beta \ket{1})_B \\
+ & \ket{\Psi ^-}_{UA}(\alpha \ket{1} - \beta \ket{0})_B]
\end{split}
\end{equation}
\begin{equation} \label{cqt-joint2}
\begin{split}
\ket{\psi}_{UAB2} = \frac{1}{2}[&\ket{\Phi ^+}_{UA}(\alpha \ket{1} + \beta
\ket{0})_B \\
+ & \ket{\Psi ^+}_{UA}(\alpha \ket{0} + \beta \ket{1})_B \\
+ & \ket{\Phi ^-}_{UA}(\alpha \ket{1} - \beta \ket{0})_B \\
+ & \ket{\Psi ^-}_{UA}(\alpha \ket{0} - \beta \ket{1})_B].
\end{split}
\end{equation}
Alice then performs a Bell-state measurement on her two qubits $U$ and $A$,
and sends the two-bit classical information to Bob. Note that we now have eight
possible outcomes at Bob's side due to the participation of Charlie, leaving
eight quantum gate combinations: $I,X,Z,XZ,X,XX(I),XZ$ and $XXZ(Z)$. After
utilising the three bits of information Bob can finally recover the initial
unknown state $\ket{\phi}_U$.

With this controlled quantum teleportation those authors were able to
implement a controlled quantum direct communication protocol. As the term direct
communication suggests, this kind of communication does not require secret key
distribution, instead Alice encode her message directly into quantum states
$\ket{+}$ and $\ket{-}$ i.e. $\frac{1}{\sqrt{2}}(\ket{0}+\ket{1})$ and
$\frac{1}{\sqrt{2}}(\ket{0}-\ket{1})$. The most obvious benefit of this
protocol is that the information sent by Alice is never revealed to a
third-party, eliminating the potential threat of a suspicious server at
Charlie's side. As the paper states, this protocol is said to be completely
secure as long as a perfect quantum channel is provided. There was a
similar
method published in 2006 which uses W state for the three distributed entangled
qubits\cite{qsdc-w}. W state is said to be infrangible in the case of qubit
loss, compared to GHZ state where the remaining two qubits will be no longer 
in entangled state when one in three of them is lost. The so-called symmetric W
state is written as:
\begin{equation} \label{W-state}
\ket{W}_{123} = \frac{1}{\sqrt{3}}(\ket{100}+\ket{010}+\ket{001})_{123}
\end{equation}
with the orthogonal basis of $\ket{0}$ and $\ket{1}$.


The work in\cite{qsdc-superdense} explained a more complex QSDC scheme which
promotes the application of a QSDC network, where the sender and receiver
communicate via a server to enhance security. Figure~\ref{fig:qsdc-superdense}
shows a subnet of this kind of network in a loop topological structure.
\begin{figure}[here]
\centering
\includegraphics[keepaspectratio=true,width=9cm]{qsdc-superdense-bw.png}
\caption{QSDC network subsystem communication protocol. Source:
\cite{qsdc-superdense}}
\label{fig:qsdc-superdense}
\end{figure}
The protocol has a thorough security check of communication channels, mainly
focuses on eavesdropping detection. The checking process is explained as
following:
\begin{enumerate}
\item The server Alice prepares a sequence of entangled qubit pairs, and for
every pair take one of the qubits, store them in a sequence, say $S_A$, where
the other side of the pairs are stored in $S_B$. Alice then send $S_B$ to the
receiver Charlie and keeps $S_A$.
\item Charlie conducts the first security check with Alice using $S_B$ to
detect eavesdropping by measuring the error rate.
\item Charlie then encrypt $S_B$ using some local unitary operations $U_C$, 
and send to the sender Bob for the second security check.
\item If the noise rate is acceptable, Bob encodes his message into $S_B$ with
operations $U_B$ and send to the server Alice.
\item Joint Bell-state measurement on $S_A$ and $S_B$ is performed by Alice,
and the classical bit result is shared with Charlie. With the given data and
information of $U_C$, Charlie is able to calculate the operations Bob
performed, which was $U_B$, thus obtaining the original message.
\end{enumerate}
Within the process any unacceptable noise rate will result in a complete
restart of the protocol, hence quantum error correction codes are
needed to mitigate the channel noise.

The authors in\cite{qsdc-superdense} also states that ``high-dimensional
quantum communication protocols provide better security than that obtainable
with two-dimensional quantum systems," because ``the two authorized users can
use more than two sets of unbiased measuring bases to check eavesdropping".
According to their theoretical formula the error rate caused by eavesdropping
will be doubled in their subsystem compared to that of BB84 quantum key
distribution protocol. Furthermore, since the receiver uses local operations
$U_C$ to encode $S_B$, Alice cannot obtain the original message sent by Bob,
avoiding the latent dishonest server. It is also worth mentioning that this
method was developed specifically for quantum communication with superdense
coding\cite{wiki-superdense}, however as we can see, in principle it could be
adopted to quantum teleportation with little modification.

\section{Quantum teleportation simulation}
\subsection{Quantum gates and quantum circuits}
In this section we briefly introduce how qubits are manipulated in
theory, which leads to the concepts of quantum gates and
circuits\cite{qit-book}. A qubit can be physically transformed by applying
unitary matrices (or operators). Such transformations are synthesised with
\emph{Pauli matrices} and are all reversible. The term \emph{reversible} means
the output of an operation can be reversed to obtain its input data, which is
obviously the opposite of what we know in practical electric circuits.
According to those operators we introduce new types of gates $I$, $X$, $Z$, $H$
and $CNOT$ that are used in quantum teleportation. 

Quantum gates are mathematically represented in Pauli matrices: 
\begin{equation}
\begin{split}
&I = \begin{pmatrix}
1 & 0\\
0 & 1\\
\end{pmatrix}\;\;
X = \begin{pmatrix}
0 & 1\\
1 & 0\\
\end{pmatrix}\;\;
Z = \begin{pmatrix}
1 & 0\\
0 & -1\\
\end{pmatrix}\\
&H = \frac{1}{\sqrt{2}}\begin{pmatrix}
1 & 1\\
1 & -1\\
\end{pmatrix}
\equiv \frac{1}{\sqrt{2}}(X + Z)
\end{split}
\end{equation}

Consider we have a qubit in the form $\ket{\phi} = \alpha \ket{0} + \beta 
\ket{1}$, by applying those gates we have: 
\begin{equation}
I\ket{\phi} = \begin{pmatrix}1 & 0 \\ 0 & 1\end{pmatrix}
\begin{pmatrix}\alpha \\ \beta\end{pmatrix} \equiv \alpha \ket{0} + \beta 
\ket{1}
\end{equation}
\begin{equation}
X\ket{\phi} = \begin{pmatrix}0 & 1 \\ 1 & 0\end{pmatrix}
\begin{pmatrix}\alpha \\ \beta\end{pmatrix} \equiv \beta \ket{0} + \alpha 
\ket{1}
\end{equation}
\begin{equation}
Z\ket{\phi} = \begin{pmatrix}1 & 0 \\ 0 & -1\end{pmatrix}
\begin{pmatrix}\alpha \\ \beta\end{pmatrix} \equiv \alpha \ket{0} - \beta 
\ket{1}
\end{equation}
\begin{equation}
\begin{split}
H\ket{\phi} & = \frac{1}{\sqrt{2}}(X\ket{\phi}+Z\ket{\phi})\\
& = \frac{1}{\sqrt{2}}[\alpha(\ket{0}+\ket{1})+\beta({\ket{0}-\ket{1}})]\\
& \equiv \alpha\ket{+} + \beta\ket{-}
\end{split}
\end{equation}
We can easily see that $I$ gate preserves the input, and $X$ gate swaps the 
amplitudes. $Z$ gate applies a $\pi$ phase shift to the two states, due to the result 
can also be represented as $\alpha\ket{0} + e^{i\pi}\beta\ket{1}$. The name of 
$H$ gate is Hadamard matrix gate, where its function is to transform any qubit 
into a superposition state $\alpha\ket{+} + \beta\ket{-}$. 
\begin{figure}[bp]
\centering
\includegraphics[keepaspectratio=true,width=9cm]{example-circuit.png}
\caption{Quantum circuit of a CCU gate. Source\cite{qit-book}.}
\label{fig:example-circuit}
\end{figure}

Controlled-NOT gate, or CNOT, is more special since it requires two inputs and 
gives two outputs. The first qubit, called the \emph{control qubit}, decides whether 
the other \emph{target qubit} is flipped (or amplitudes swapped if in 
superposition). For the matrices and complete truth table refer to pages 316 
to 318 in \cite{qit-book}. Derived from the idea of CNOT, a new category called
controlled-$U$ gates were invented. $U$ can be any quantum gate with $2 \times 
2$ Pauli matrix, such as $X$, $Z$ and $H$. The control qubit decides whether 
the $U$ gate is applied to the target qubit. 

Quantum circuits model processes in quantum computing by using a sequence of 
quantum gates. Figure~\ref{fig:example-circuit} is an example circuit that functions as a 
controlled-controlled-$U$ (CCU) gate\cite{qit-book}, assuming we have a $V$ gate such 
that $VV^+ = I$ and $V^2 = U$. 
The dot-cross notation stands for CNOT gate, with dot represents the control 
bit. In a similar way a dot followed by a gate stands for a controlled-$U$ gate. 


\subsection{Simulation program}
The simulation is realised with a C++ program that uses 
%IT++ library\cite{itpp} for communication simulation and 
Dlib\cite{dlib} for quantum computing simulation. Figure~\ref{fig:qt-circuit}
shows the overall quantum circuit the program simulates. 

\begin{figure}[here]
\centering
\includegraphics[keepaspectratio=true,width=9cm]{qt-circuit.png}
\caption{Quantum teleportation full circuit}
\label{fig:qt-circuit}
\end{figure}

The shaded area in the graph is the apparatus that entangles $\ket{\psi}_{12}$, 
and the other parts involves $\ket{\phi}$ and $\ket{\psi}_1$ function as a 
Bell-state measurement, which also is an entanglement process. The 
graphical representation in Figure~\ref{fig:measurement}
\begin{figure}[here]
\centering
\includegraphics[keepaspectratio=true,width=1.cm]{measure.png}
\caption{Qubit measurement}
\label{fig:measurement}
\end{figure}
stands for a qubit measurement process. It gives a classical bit result, while 
the qubit is disturbed (collapse into a pure state) as a consequence. After the
measurement, two classical bits are sent to the lane where controls the qubit 
$\ket{\psi}_2$ at Bob's side. We can also regard the two gates as 
controlled-$X$ and controlled-$Z$ gates, which transform $\ket{\psi}_2$ 
according to the measurement outcomes. 

The program simulates quantum teleportation by strictly following the circuit. 
A random qubit $\ket{\phi}$ is firstly generated, followed by adding two empty 
qubits $\ket{0}$ that stand for $\ket{\psi}_{12}$. Extra care was taken 
while generating the random qubit, since we had to preserve the axiom 
$|\alpha|^2 + |\beta|^2 = 1$. This was solved by firstly generate a random complex 
number with length less than one then calculate a corresponding $\beta$. 
Because we only have the information of $\beta$'s length, the angle of $\beta$ 
is a random degree in the range of $0^\circ$ to $360^\circ$. The program was also adapted to have 
high efficiency when it is ran repetitively, which is an important property since there are potential usages of 
quantum teleportation in some other larger quantum computing simulation system. 
In order to function well with fast repetitive execution the random seed was set to 
microsecond of current time. 

\section{Simulation Results}
In this section we present outputs of the program and validate its 
correctness. Before went into repetitive executions the correctness of individual steps 
was checked. Figure~\ref{fig:tele-simu} shows the program output of three possible 
outcomes of the Bell-state measurement. 
\begin{figure}[htbp]
\centering
\begin{subfigure}[b]{0.5\textwidth}
\centering
\includegraphics[width=\textwidth]{tele-simu1.png}
\caption{Result 1}
\label{fig:tele-simu1}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\centering
\includegraphics[width=\textwidth]{tele-simu2.png}
\caption{Result 2}
\label{fig:tele-simu2}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\centering
\includegraphics[width=\textwidth]{tele-simu3.png}
\caption{Result 3}
\label{fig:tele-simu3}
\end{subfigure}
\caption{Quantum teleportation simulation with different measurement results.}
\label{fig:tele-simu}
\end{figure}
What we print is the state vector of 
qubits in a quantum register (i.e. the tensor product of all qubits. Refer to 
page 327 in\cite{qit-book} for details of tensor product in qubits). For instance for the
first two lines of output, which is the random qubit, the two complex numbers stand for the 
values of $\alpha$ and $\beta$ in $\ket{\phi} = \alpha\ket{0}+\beta\ket{1}$. 
In a similar way, after appending the two empty qubits $\ket{0}$ to the random 
qubit we have: 
\begin{equation}
\begin{split}
\ket{\phi}\ket{\psi}_{12} = &a\ket{000}+b\ket{001}+c\ket{010}+d\ket{011}\\
+&e\ket{100}+f\ket{101}+g\ket{110}+h\ket{111}, 
\end{split}
\end{equation}
and we can see in the outputs only $a$ and $e$ have values other than 0, which 
is correct. After the Bell-state measurement $\ket{\phi}$ and $\ket{\psi}_1$ 
are removed from the quantum register, thus we can obtain only the state vector 
of $\ket{\psi}_2$. The final output was given after applying the corresponding 
quantum gates, and there we can see the qubit amplitudes of $\ket{\psi}_2$ become 
exactly the same with that of the initial random qubit. This concludes our 
program flow check (white-box testing). 

\begin{figure}[htbp]
\centering
\includegraphics[width=0.5\textwidth]{tele-simu-iter.png}
\caption{Success rate of teleporting 10000 qubits, with the last a few
checks shown in the picture. }
\label{fig:tele-simu-iter}
\end{figure}

We then performed a reliability check of the program, regarding it as a 
module in some larger system (black-box testing). The module was left to run 
in a large number of times and the state vectors of the random qubit and 
output are compared programmatically. Figure~\ref{fig:tele-simu-iter} shows 
outputs from the test. The result told that the program is able to cope with 
fast repetitive executions and provide reliable output qubits with 100\% success rate. 

\section{Future works and Conclusion}
By end of this project we found there were still moderate amount of topics 
that could be explored. The first improvement we would do if the schedule were 
more relaxing is to introduce noise to both of the classical channel and 
quantum channel, which should bring further interesting results about error 
ratio in the output qubits. This may also introduce the need of quantum error 
correction algorithms, such as the Shor code\cite{shor-code}. A further 
perspective could be the implementation of a larger quantum computing 
simulation system. In that case the current quantum teleportation module could 
be used as a data transmission protocol, or if with more effort, a quantum 
network based on teleportation is also possible, which might again refer to 
\cite{quantum-network} and \cite{qsdc-superdense}. 

In conclusion, this report functions as a step stone for fresh computer 
scientists to get started in the world of quantum information technology, more 
specifically, in the subject of quantum teleportation (QT). We introduced and 
defined QT in details, summarised its possible applications and limitations. 
The history and many current achievements were concluded and explained 
technically. Finally we presented a robust simulation program that realises this 
cutting-edge technology with own hand. 

\bibliographystyle{ieeetr}
\bibliography{ref}



\end{document}