/*
 * 2SD_test1.cpp
 *
 *  Created on: Jul 31, 2011
 *      Author: zunaira
 * ***********************************************************************************************************
 *
 * 2-Qubit Superdense Code: BER versus Pe
 *
 ***********************************************************************************************************
 *Input File: control.sim.par
 *
 */

#include <iostream>
#include <complex>
#include <ctime>
#include "dlib/quantum_computing.h"
#include "dlib/string.h"
#include "itpp/itbase.h"
#include "itpp/itcomm.h"
#include "fileser.h"

using namespace std;
using namespace dlib;
using namespace itpp;

// This declares a random number generator that we will be using below
dlib::rand rnd;
// ----------------------------------------------------------------------------------------

int main()
{
	//---------read simulation parameters
	FILE *sim_file;
	FILESERVICE fileser;
	sim_file = fopen("control.sim.par", "r");
	if (sim_file == NULL)
	{
		it_error("control.sim.par not found");
	}
	char resultfile[500]; fileser.scan_text(sim_file, resultfile);
	int N = fileser.scan_integer(sim_file); //frame size (no.of classical bits)
	double pe_start = fileser.scan_double(sim_file); //Pe start
	double pe_step = fileser.scan_double(sim_file); //Pe step-size
	double pe_end = fileser.scan_double(sim_file); //Pe end
	fclose(sim_file);
	//-END-----read simulation parameters

	char text[100];
	sprintf( text, "%f:%f:%f", pe_start, pe_step, pe_end );
	vec Pe = text; //probability of error range for simulation
	int L = length(Pe);

	//Initialize variables
	bvec b_source_bits, b_decoded_bits;
	ivec No_of_Errors, No_of_Bits, No_of_Flips;
	vec ber, err;
	//Allocate memory
	b_decoded_bits.set_size(N,false);
	No_of_Errors.set_size(L, false);
	No_of_Bits.set_size(L, false);
	No_of_Flips.set_size(L, false);
	ber.set_size(L, false);
	//clear variables
	No_of_Errors.clear();
	No_of_Bits.clear();
	No_of_Flips.clear();

	BERC berc; //BER counter

	//Quantum gates
	using namespace dlib::quantum_gates;
	const gate<1> h = hadamard();
	const gate<1> z = quantum_gates::z();
	const gate<1> x = quantum_gates::x();
	const gate<1> i = quantum_gates::noop();

	//generate entangled pair (EPR) for Superdense coding- |00> + |11>
	quantum_register EPR;
	EPR.set_num_bits(2);
	//apply hadamard gate to 1st qubit
	(h,i).apply_gate_to(EPR);
	//apply CNOT to 2nd qubit controlled by the 1st qubit
	(cnot<1,0>()).apply_gate_to(EPR);

	//Initializing Qubit pair for transmission over channel
	quantum_register ch_qubit;
	ch_qubit.set_num_bits(2);

	//create file for storing results
	FILE *f = fopen(resultfile, "w");
	fprintf(f,"! No Error Correction - 2Qubit superdense coding (Error-free EPR sharing)\n");
	fprintf(f, "!Pe \t\t No_of_Errors \t\t No_of_Bits \t\t BER \t\t Flips \n");
	fflush(f);
	printf("!Pe \t\t No_of_Errors \t\t No_of_Bits \t\t BER \t\t Flips \n");

	RNG_randomize(); //construct random source
	Uniform_RNG RNG(0.0, 1.0);

	for (int p=0; p<L; p++) //for Pe
	{
		RNG_reset(0); //reset random seed
		while (No_of_Errors(p)<1000) //accumulate at least 1000 errors
		{
			//Generate random source bits
			b_source_bits = randb(N);

			//transmission over Quantum channel using superdense coding
			for (int c=0; c<N; c=c+2)
			{
				//Superdense coding at transmitter
				ch_qubit = EPR;
				if (b_source_bits(c) == 0 && b_source_bits(c+1) == 1) // classical bit pair [0 1]
				(x,i).apply_gate_to(ch_qubit); // |10> + |01>
				else if (b_source_bits(c) == 1 && b_source_bits(c+1) == 0) // classical bit pair [1 0]
				(z,i).apply_gate_to(ch_qubit); // |00> - |11>
				else if (b_source_bits(c) == 1 && b_source_bits(c+1) == 1)// classical bit pair [1 1]
				{
					(z,i).apply_gate_to(ch_qubit); // |10> - |01>
					(x,i).apply_gate_to(ch_qubit);
				}

				//Q-channel
				err=RNG(1); //Q-channel - generate random error
				//flip 1st Qubit based on randomly generated error. 2nd Qubit is assumed to be transmitted error-free
				if (err(0)<=Pe(L-1-p)/3) //1st Qubit
				{	(x,i).apply_gate_to(ch_qubit);
					No_of_Flips(p)=No_of_Flips(p)+1;}
				else if (err(0)<=2*Pe(L-1-p)/3)
				{	(z,i).apply_gate_to(ch_qubit);
					No_of_Flips(p)=No_of_Flips(p)+1;}
				else if (err(0)<=Pe(L-1-p)) {
					(z,i).apply_gate_to(ch_qubit);
					(x,i).apply_gate_to(ch_qubit);
					No_of_Flips(p)=No_of_Flips(p)+1;}

				//Superdense decoding at receiver
				//apply CNOT to 2nd qubit controlled by the 1st qubit
				(cnot<1,0>()).apply_gate_to(ch_qubit);
				//apply hadamard gate to 1st qubit
				(h,i).apply_gate_to(ch_qubit);
				//measure qubits to convert to classical bit
				b_decoded_bits(c) = ch_qubit.measure_bit(1,rnd);
				b_decoded_bits(c+1) = ch_qubit.measure_bit(0,rnd);

			}

			berc.clear(); //Clear up buffer of BER counter
			berc.count (b_source_bits, b_decoded_bits); //Count error bits in a word
			No_of_Errors(p) += berc.get_errors(); //Updating counters
			No_of_Bits(p) += berc.get_errors()+berc.get_corrects();
		}
		ber(p) = (double)No_of_Errors(p)/No_of_Bits(p);

		fprintf(f,"%f\t\t%i\t\t%i\t\t%f\t\t%i\n",Pe(L-1-p),No_of_Errors(p),No_of_Bits(p),ber(p),No_of_Flips(p));
		fflush(f);
		printf("%f\t\t%i\t\t%i\t\t%f\t\t%i\n",Pe(L-1-p),No_of_Errors(p),No_of_Bits(p),ber(p),No_of_Flips(p));
		if(ber(p)<1e-5) //BER stop
		break;
	}
	fclose(f);
}
