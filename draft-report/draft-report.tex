\documentclass[journal,compsoc]{IEEEtran}

\usepackage{comment}
\usepackage{cite}
\usepackage[pdftex]{graphicx}
\graphicspath{{pic/}}
\usepackage{braket}
\hyphenation{real-ised}
\usepackage{fancyref}
\usepackage{hyperref}
\usepackage{breakurl}
\usepackage[outerbars]{changebar}
\usepackage{amsmath}

\begin{document}
\title{\bf Research, Evaluation and Simulation of Quantum Teleportation}
\author{Weike~Liao,~\IEEEmembership{Student,}
        Soon~X.~Ng,~\IEEEmembership{Supervisor}
\IEEEcompsocitemizethanks{\IEEEcompsocthanksitem W. Liao is with the School of Electronics and Computer Science, University of Southampton, United Kingdom.\protect\\
E-mail: wl15g09@ecs.soton.ac.uk
\IEEEcompsocthanksitem S.X. Ng is with the group of Communications, Signal Processing and Control, School of Electronics and Computer Science, University of Southampton, United Kingdom.\protect\\
Email: sxn@ecs.soton.ac.uk}}

\IEEEcompsoctitleabstractindextext{
\begin{abstract}

In this report the concept of quantum teleportation is introduced, with explanations about its motivation and possible applications. Previous and current research achievements are summarised and evaluated in the author's perspective. A simulation program of quantum teleportation was implemented, tested and evaluated, giving constructive conclusions in the end. 
\end{abstract}

\begin{IEEEkeywords}
quantum, teleportation, entanglement, simulation, mechanics, evaluation
\end{IEEEkeywords}}

\maketitle
\section{Introduction}
\IEEEPARstart{T}{he} subject of quantum information theory (QIT) nowadays is a fast developing area with increasing investigations and researches around the globe. In this report we will focus on a subarea in QIT named quantum teleportation (QT)\cite{quantum-network,quantum-teleportation-100,quantum-teleportation-143,quantum-computing,qt-epr}. The objective of QT is to transmit an unknown quantum state over a distance, and it is one of the few quantum communication protocols invented so far (another one is superdense coding)\cite{quantum-network}.

\cbstart One of the most important elements in transition from classical information theory to QIT is to understand the concept of quantum bit, or qubit\cite{qit-book}. A qubit represents the state of a quantum system with two orthogonal basis: $\ket{0}$ and $\ket{1}$, where the special symbols are called bra-ket notation\cite{wiki-braket}. An interesting property of qubit is its probabilistic superposition of two states, which says it can hold $\ket{0}$ and $\ket{1}$ at the same time with certain probabilities to fall into one of the state when is measured. This means, we never know a qubit's true state before measuring it, while any measurement behaviours will break the superposition state into a pure state (distinctive $\ket{0}$ or $\ket{1}$). The notation of a qubit is written as:
\begin{equation} \label{qubit}
\ket{\psi} = \alpha \ket{0} + \beta \ket{1}, 
\end{equation} 
where $\alpha$ and $\beta$ are probabilities and add up to 1. \cbend 

\begin{figure}[here]
\centering
\includegraphics[keepaspectratio=true,height=7cm]{qt.png}
\caption{Quantum teleportation}
\label{fig:qt}
\end{figure}
The theoretical report of quantum teleportation was first published in 1993 by six scientists\cite{qt-epr}. They proved the teleportation of a qubit $\ket{\phi}$ from ``Alice" to ``Bob" is possible by destroying the qubit which Alice at hand then restore it at Bob's side, without obtaining any information about the quantum state $\ket{\phi}$. The process can be explained as follows. Say Alice has three qubits \cbstart in photon system\cbend , in which two of them $\ket{\psi_1}$ and $\ket{\psi_2}$ are entangled in the Bell state\cite{wiki-entangle} \cbstart in polarisation: \cbend 
\begin{equation} \label{eq:epr-pair}
\ket{\Psi^-_{12}} = \sqrt{\frac{1}{2}}(\ket{\uparrow_1}\ket{\downarrow_2} - \ket{\downarrow_1}\ket{\uparrow_2})
\end{equation}
 and another one $\ket{\phi}$ is in an unknown state which she will send its information to Bob:
\begin{equation} \label{eq:unknown-state}
\ket{\phi} = a\ket{\uparrow} + b\ket{\downarrow}.
\end{equation}
\cbstart The arrows stands for the polarisation direction of the quantum state. \cbend Firstly Alice sends one of the entangled qubit to Bob via some quantum channel, then entangle the remaining two qubits by performing a joint Bell-state measurement\cite{bell-measure}. The complete state of the three qubits is now:
\begin{equation} \label{eq:3-qubits1}
\begin{split} 
\ket{\Psi_{123}} = & \frac{a}{\sqrt{2}}(\ket{\uparrow_1}\ket{\downarrow_2}\ket{\uparrow_3} - \ket{\downarrow_1}\ket{\uparrow_2}\ket{\uparrow_3}) \\ & + \frac{b}{\sqrt{2}}(\ket{\uparrow_1}\ket{\downarrow_2}\ket{\downarrow_3} - \ket{\downarrow_1}\ket{\uparrow_2}\ket{\downarrow_3}).
\end{split}
\end{equation}
Subscript 3 stands for the qubit of the unknown state. According to the Bell state basis we can then rewrite the equation into: 
\begin{equation} \label{eq:3-qubits2}
\begin{split} 
\ket{\Psi_{123}} = \frac{1}{2}[& \ket{\Psi^-_{13}}(-a\ket{\uparrow_2} - b\ket{\downarrow_2}) \\ & + \ket{\Psi^+_{13}}(-a\ket{\uparrow_2} + b\ket{\downarrow_2}) \\ & + \ket{\Phi^-_{13}} (a\ket{\downarrow_2} + b\ket{\uparrow_2}) \\ & + \ket{\Phi^+_{13}}(a\ket{\downarrow_2} - b\ket{\uparrow_2})].
\end{split}
\end{equation}
This explains that, there should be four possible measurement results with equal probability of 1/4. At this step the qubit $\ket{\psi_2}$, which Bob has, is supposed to be projected into one of the four pure states shown in equation~\ref{eq:3-qubits2}. It is not hard to see that $\ket{\psi_2}$ now has very similar state to the original $\ket{\phi}$, where one of the cases is exactly the same. In other words, the teleportation is already successful with 25\% chance. With the other three cases we can perform unitary transformation to $\ket{\psi_2}$ as long as the measurement outcome at Alice's side is provided, which finally produces an exact replica of the destroyed $\ket{\phi}$. The process of sending Alice's measurement information is called active feed-forward, and is usually achieved with a classical data channel. The whole process of quantum teleportation is also illustrated in Figure~\ref{fig:qt}. 

Following that discovery other scientists realised the idea and demonstrated QT experiments in various systems such as photons, nuclear spins and trapped ions\cite{qt-ibm,exp-qt}. Photons are found to have little decoherence from noisy environment and are easy to manipulate as well as transmit, thus the early methods of distributing photons were mainly through optical fibres. Although experiments with fibres were successful, even with cutting-edge techniques\cite{qt-danube}, optical fibres were still left behind due to their high photon loss and decoherence factor. Instead free-space optical link was proved to be the successor, mainly because of its low negative effects from atmosphere, which brings expressively longer transmission distance (144 kilometers maximum so far)\cite{exp-fs-qt,quantum-teleportation-100,quantum-teleportation-143}. 


\section{Motivation and possible applications}

Quantum computing is the foreseeable future of computer science, it utilises quantum mechanical phenomena and relies on quantum information theory. The direct communication in qubits between different individuals is an essential part in distributed quantum computing, which also introduces the need for a global quantum network. However, the sending of quantum states in classical means is impossible, which can be explained in the following arguments\cite{wiki-qt}:
\begin{itemize}
\item Physical transport of qubits. \\
Fragile quantum states would be easily corrupted by very little interruptions during the way, which is not recommended. 
\item Broadcast of quantum information. \\
No-broadcast theorem, a corollary of the no-cloning theorem, states that it is impossible to create a state that both of its parts are the same as the original state. 
\item Measure the quantum going to be sent, then send its information in classical manner and creates a new quantum on the receiver side according to the information. \\
Since we can only perform a single measurement of any quantum before disturbing it, and due to Heisenberg's uncertainty principle, we cannot measure the complete states of a quantum. This is also explained in the no-teleportation theorem\cite{wiki-no-tele}. 
\end{itemize}
Quantum teleportation is the first and only solution so far, it gives the feasibility to transfer unknown quantum states over long distances. The first realistic application of this technology is in quantum cryptography.

Nowadays the most popular and reliable method for data encryption is to separate a private encryption key depending on the data, and transfer the key individually. Quantum cryptography\cite{quantum-crypt} is a rather old field compared to QT, its main idea is to transmit the private key via quantum states, where majority experiments uses polarisation of photons. The most famous protocol, BB84\cite{quantum-crypt}, is a method that calculates private key on both sender and receiver sides, depending on the photon polarisations measured.  It is said to be immune to eavesdropping because interception and retransmission by the eavesdropper can not extract useful data. However, in that case, the transmission will be disturbed and the noise ratio greatly increases. Quantum teleportation solves this problem by completely forbidding eavesdropping and is called ``the ultimate solution to quantum cryptography"\cite{quantum-crypt}. 

Apart from quantum key distribution (QKD), the field of quantum secure direct communication (QSDC) is being actively researched during recent years\cite{qsdc-superdense,dsdc-entang-swap,qsdc,qsdc-w,qsdc-epr-qt}, where many of them use teleportation. The QSDC protocol invented by F.G. Deng \emph{et al.} in 2008\cite{qsdc-superdense} summarised previous techniques and produced a novel method that uses superdense coding. The protocol inherited some merits from that in T. Gao \emph{et al.}'s teleportation protocol\cite{qsdc}, and could be adopted with QT as well. They claimed in their method that any eavesdropping behaviours will be twice easier to detect compared to previous BB84 QKD protocol, for the reason that those behaviours will double the error rate. 

Another discovery in quantum teleportation is entanglement swapping. This technique achieves the entanglement of qubits that never interacted. Consider the following case as shown in Figure~\ref{fig:entang-swap}, we have four qubits, $\ket{\phi}_1$, $\ket{\phi}_2$, $\ket{\psi}_1$ and $\ket{\psi}_2$ distributed as the graph illustrates. $\ket{\phi}_1$ and $\ket{\phi}_2$ are entangled and so are $\ket{\psi}_1$ and $\ket{\psi}_2$, where $\ket{\phi}_2$ and $\ket{\psi}_1$ are in the same place.  
The idea is, in short, to teleport the state of $\ket{\phi}_2$ to $\ket{\psi}_2$ by performing projective measurement (Bell-state measurement\cite{bell-measure}) on $\ket{\phi}_2$ and $\ket{\psi}_1$. As a result, $\ket{\phi}_1$ and $\ket{\psi}_2$ are now entangled\cite{entangle-swap}. This technique was applied in quantum repeater\cite{qurep} and explored to fit with a secure direct communication protocol\cite{dsdc-entang-swap}. 
\begin{figure}[here]
\centering
\includegraphics[keepaspectratio=true,width=9cm]{entang-swap.png}
\caption{Entanglement swapping}
\label{fig:entang-swap}
\end{figure}

In the research of quantum network, QT can be used as a communication protocol at the cost of an EPR pair and two classical bits per qubit\cite{quantum-network}. This requires entanglement distribution by routers between Alice and Bob who do not share direct connections, which means it is possible to establish virtual connections between nodes without a direct quantum channel between them\cite{quantum-network}. 

\section{Challenges}

A major challenge in quantum teleportation is the significant short coherence time between entangled pairs. When a qubit is in superposition, that is, it simultaneously holds values 1 and 0, then it is said to be in coherence. Any perturbation from the environment will result the qubit collapse into a pure state and terminate the entanglement\cite{entang-decoh}. At the current level of research, we have to use entanglement distribution via optical channels, which is a delicate and challenging task in QT, it is also one of the main reasons that make QT not a feasible solution to the current field of quantum communication\cite{quantum-teleportation-143}. This problem will hopefully be solved with the forthcoming technology of quantum memory. In this case, entangled quanta can be stored in separate quantum memories and be kept at different locations for significantly longer time, which brings future QT further advantages: 
\begin{itemize}
\item QT is available even when quantum channel has low quality, or
\item When the location of Bob is unknown, means a broadcast of classical channel from Alice. 
\end{itemize}
The current best achievement in practical quantum memory is of 1.75s decoherence time with 90\% fidelity, which is still far from what QT requires\cite{quantum-memo-nuclear}. The authors developed a coherence transfer scheme that involves a processing qubit (electron spin) and a memory qubit (nuclear spin). In the writing process, a coherent electron spin state is generated then stored into a nuclear spin, and vice versa for the reading process.  


Experimental free-space long-distance QT face many difficulties, especially in the manipulation of quantum channel. For instance, one is the extremely low signal-to-noise ratio. To cope with this, a successful experiment in 2012 utilised: frequency-uncorrelated polarization-entangled photon pair source, ultra-low-noise large-active-area single-photon detectors and entanglement-assisted clock synchronisation, while the experiments could only be carried out during night\cite{quantum-teleportation-143}. 

\section{History and current development}
Starting with the first discovery of quantum teleportation by Bennett \emph{et al}\cite{qt-epr}, significant amount of research has been done in the relevant areas in all over the world. It is worth mention that theoretical discoveries are far beyond what have been achieved experimentally. The first experimental QT\cite{exp-qt} happened in 1997, they used photon pairs entangled in polarisation. At that time the techniques for any kind of entanglement were immature, for which they used a method called type-II parametric down-conversion that produces entangled photons in orthogonal polarisations\cite{para-down-conv}. However in that experiment they did not use classical information at the Bob's side, that means only one case in the four possible Bell-state measurement outcomes is considered (the case where Bob need to apply an {\bf I} gate). In the same year a theoretical report\cite{qt-conti-var} explained the process to teleport continuous spectra (``manifestly quantum sates of electromagnetic field"), with two ancillary highly squeezed two-mode electromagnetic fields as the entangled states, which was a valuable step in QT from discrete-variable systems to continuous-variable systems. They also managed to obtain accurate fidelity of entanglement in an infinite dimensional Hilbert space with absolute efficiency, compared to parametric down-conversion which only has rare success rate. In the following year a method of QT with a Greenberger-Horne-Zeilinger state triplet\cite{ghz} was published\cite{qt-3-entang}. In that experiment three distant entities were involved, and one can teleport an unknown state to either of other two places. That technique was further expanded in 2004\cite{multi-qt}. The authors formulated a technique to teleport strings of qubits from one location to another via a many-agent network. A notable improvement was that, regardless of the amount of qubits to be teleported, every agent in the network only need one GHZ state qubit, perform one Hadamard gate transform and send one classical bit to the receiver. 

The multiqubit teleportation scheme was also explored in the context of secure transmission in that year. A secure direct communication protocol using controlled QT was discovered\cite{qsdc}. The GHZ state triplet was again used, with the third entity Charlie as a supervisor of the transmission between Alice and Bob. As the term direct communication suggests, this kind of communication does not require secret key distribution, instead Alice encode her message directly into quantum states $\ket{+}$ and $\ket{-}$. The method is said to be completely secure as long as a perfect quantum channel is provided. A similar method uses W state was published in 2006 as well\cite{qsdc-w}. 

\cbstart \section{Quantum teleportation simulation}
The simulation is realised with a C++ program, which uses IT++ library\cite{itpp} for communication simulation and Dlib\cite{dlib} for qubit manipulation. Figure~\ref{fig:qt-circuit} shows the overall quantum circuit the program simulates. 
\begin{figure}[here]
\centering
\includegraphics[keepaspectratio=true,width=9cm]{qt-circuit.png}
\caption{Quantum teleportation full circuit}
\label{fig:qt-circuit}
\end{figure}
\cbend 


\begin{comment}


\end{comment}
\section*{Acknowledgements}
%The authors would like to thank...
\appendices
\section{First Appendix}
\bibliographystyle{ieeetr}
\bibliography{ref}



\end{document}